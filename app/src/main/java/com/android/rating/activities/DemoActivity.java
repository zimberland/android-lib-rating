package com.android.rating.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.android.rating.R;
import com.android.rating.utils.IntentHelper;
import com.android.rating.utils.RatingUtils;

import java.util.List;


public class DemoActivity extends Activity implements AdapterView.OnItemSelectedListener {

    Spinner spinner;
    List<String> allTypes;
    IntentHelper intentHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
        intentHelper = new IntentHelper(this);
        initializeControls();
    }

    private void initializeControls() {
        spinner = (Spinner) findViewById(R.id.rating_type);
        allTypes = RatingUtils.getRatingTypes(getString(R.string.spinner_prompt));
        ArrayAdapter<String> ratingTypes = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item,
                allTypes);
        spinner.setAdapter(ratingTypes);
        spinner.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 1:
                intentHelper.startStandardRatingActivity();
                break;
            case 2:
                intentHelper.startSessionRatingActivity();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Do nothing
    }
}
