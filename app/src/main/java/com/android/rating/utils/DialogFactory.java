package com.android.rating.utils;


import android.app.ProgressDialog;
import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.android.lib.rating.connector.RatingConnector;
import com.android.lib.rating.enums.RatingType;
import com.android.rating.R;
import com.android.rating.activities.RatingBySessionActivity;
import com.android.rating.activities.StandardRatingActivity;
import com.android.rating.utils.IntentHelper;
import com.android.rating.widget.DemoDialog;

public class DialogFactory {

    private Context context;
    private ProgressDialog progressDialog;
    private IntentHelper intentHelper;

    public DialogFactory(Context context) {
        this.context = context;
        intentHelper = new IntentHelper(context);
    }

    public void displayAlertDialog(String message) {

        final DemoDialog demoAlertDialog = new DemoDialog(context);

        demoAlertDialog.setMessage(message);
        demoAlertDialog.setCancelable(false);

        demoAlertDialog.setPositiveButton(R.string.yes, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "TODO : Open google play store", Toast.LENGTH_LONG).show();
                demoAlertDialog.dismiss();
                intentHelper.startDemoActivity();

            }
        });

        demoAlertDialog.setNeutralButton(R.string.later, new View.OnClickListener() {

            @Override
            public void onClick(final View v) {
                if(context instanceof RatingBySessionActivity){
                    RatingConnector ratingConnector =
                            ((RatingBySessionActivity) context).getRatingConnector();
                    ratingConnector.resetRatingSession();
                }
                if(context instanceof StandardRatingActivity){
                    RatingConnector ratingConnector =
                            ((StandardRatingActivity) context).getRatingConnector();
                    ratingConnector.resetStandardRating();
                }
                demoAlertDialog.dismiss();
            }
        });

        demoAlertDialog.setNegativeButton(R.string.no, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Thanks", Toast.LENGTH_LONG).show();
                demoAlertDialog.dismiss();
                intentHelper.startDemoActivity();
            }
        });
        demoAlertDialog.show();
    }

    public void showProgressDialog(String message) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }
}
