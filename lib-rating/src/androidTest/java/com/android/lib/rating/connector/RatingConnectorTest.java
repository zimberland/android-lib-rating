package com.android.lib.rating.connector;

import android.content.Context;

import com.android.lib.rating.enums.RatingType;
import com.android.lib.rating.listener.SessionListener;
import com.android.lib.rating.listener.StandardRatingListener;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

@RunWith(RobolectricTestRunner.class)
public class RatingConnectorTest {

    public static final int BUILDER_LAUNCH_NUMBER = 5;
    public static final int BUILDER_DURATION = 10000;
    Context context;
    RatingConnector ratingConnector;
    StandardRatingListener standardRatingListener = new StandardRatingListener() {
        @Override
        public void onStandardRatingMaxLaunchReach() {}

        @Override
        public void onResetStandardRating() {}
    };

    SessionListener sessionListener = new SessionListener() {
        @Override
        public void onSessionDurationEnd() {}

        @Override
        public void onSessionDurationReset() {}
    };

    @Before
    public void setUp() {
        context = RuntimeEnvironment.application;
    }

    @Test
    public void shouldReturnStandardRatingAsDefaultType() {
        ratingConnector = new RatingConnector(context);
        Assert.assertEquals(ratingConnector.getRatingType(), RatingType.STANDARD);
    }

    /** Standard Rating */

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenStandardListenerNotSet() {
        ratingConnector = new RatingConnector.LaunchBuilder(context)
                .setBuilderLaunchNumber(BUILDER_LAUNCH_NUMBER)
                .setBuilderStandardListener(null)
                .buildStandardRating();
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenLaunchNumberNotSet() {
        ratingConnector = new RatingConnector.LaunchBuilder(context)
                .setBuilderStandardListener(standardRatingListener)
                .buildStandardRating();
    }

    @Test
    public void shouldInitializeLaunchBuilder() {
        ratingConnector = new RatingConnector.LaunchBuilder(context)
                .setBuilderLaunchNumber(BUILDER_LAUNCH_NUMBER)
                .setBuilderStandardListener(standardRatingListener)
                .buildStandardRating();

        Assert.assertNotNull(ratingConnector);
        Assert.assertEquals(ratingConnector.getLaunchNumber(), BUILDER_LAUNCH_NUMBER);
    }

    /** Session Rating */

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenSessionListenerNotSet() {
        ratingConnector = new RatingConnector.SessionBuilder(context)
                .setBuilderDuration(BUILDER_DURATION)
                .buildSession();
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenDurationNotSet() {
        ratingConnector = new RatingConnector.SessionBuilder(context)
                .setBuilderSessionListener(sessionListener)
                .buildSession();
    }

    @Test
    public void shouldInitializeSessionBuilder() {
        ratingConnector = new RatingConnector.SessionBuilder(context)
                .setBuilderDuration(BUILDER_DURATION)
                .setBuilderSessionListener(sessionListener)
                .buildSession();

        Assert.assertNotNull(ratingConnector);
        Assert.assertEquals(ratingConnector.getDuration(), BUILDER_DURATION);
    }
}
